import unittest
from BowlingGame import BowlingGame
import pytest

class TestBowlingGame(unittest.TestCase):
    def testCreateGame(self):
        game = BowlingGame()

    def test_the_score_starts_at_zero(self):
        game = BowlingGame()
        assert game.scoreInit() == 0

    def test_rolling_less_than_zero_is_not_allowed(self):
        game = BowlingGame()
        with pytest.raises(ValueError) as e:
            game.roll(-1)
        assert str(e.value) == "Cannot roll less than zero pins."

    def test_rolling_greater_than_ten_is_not_allowed(self):
        game = BowlingGame()
        with pytest.raises(ValueError) as e:
            game.roll(11)
        assert str(e.value) == "Cannot roll more than ten pins."

    def test_a_single_roll(self):
        game = BowlingGame()
        game.roll(5)
        assert game.score() == 5

    def test_two_rolls_without_spare(self):
        game = BowlingGame()
        game.roll(5)
        game.roll(3)
        assert game.score() == 8

    def test_more_than_ten_pins_in_a_frame(self):
        game = BowlingGame()
        game.roll(5)
        with pytest.raises(ValueError) as e:
            game.roll(6)
        assert str(e.value) == "You can only clear 10 pins in a single frame."
    
    def test_three_rolls(self):
        game = BowlingGame()
        game.roll(5)
        game.roll(3)
        game.roll(7)
        assert game.score() == 15

    def test_spare_includes_bonus_roll(self):
        game = BowlingGame()
        game.roll(5)
        game.roll(5)
        game.roll(7)
        assert game.score() == 24

    def test_spare_may_have_no_bonus(self):
        game = BowlingGame()
        game.roll(5)
        game.roll(5)
        game.roll(0)
        assert game.score() == 10

    def test_strike_includes_first_bonus_roll(self):
        game = BowlingGame()
        game.roll(10)
        game.roll(7)
        assert game.score() == 24

    def test_spare_missing_next_roll(self):
        game = BowlingGame()
        game.roll(5)
        game.roll(5)
        assert game.score() == 10
    
    def test_strike_could_be_followed_by_another_strike(self):
        game = BowlingGame()
        game.roll(10)
        game.roll(10)
        game.roll(2)
        assert game.score() == 36

    def test_too_many_rolls(self):
        game = BowlingGame()
        for roll in range(20):
            game.roll(3)
            
        with pytest.raises(ValueError) as e:
            game.roll(5)
        assert str(e.value) == "Cannot bowl more than 10 frames."
    
    def test_too_many_rolls_on_frame_eleven(self):
        game = BowlingGame()
        for roll in range(20):
            game.roll(5)
            
        with pytest.raises(ValueError) as e:
            game.roll(5)
            game.roll(5)
            game.roll(5)
        assert str(e.value) == "Cannot bowl more than 11 frames with 1 bowl for the last frame"
    
    def test_last_frame_spare_allows_bonus_roll(self):
        game = BowlingGame()
        for i in range(18):
            game.roll(1)
        game.roll(8)
        game.roll(2)
        game.roll(3)

    def test_perfect_game(self):
        game = BowlingGame()
        for i in range(11):
            game.roll(10)
        assert game.score() == 300

# -------------------------------------------- Test multiplayer -------------------------------------------------
    def test_multiplayer_init(self):
        game = BowlingGame(2)
        assert game.player == 2

    def test_multiplayer_a_single_roll(self):
        game = BowlingGame(2)
        game.roll(5, 1)
        game.roll(6, 2)
        assert game.score(1) == 5
        assert game.score(2) == 6

    def test_two_rolls_without_spare(self):
        game = BowlingGame(2)
        game.roll(5, 1)
        game.roll(4, 2)
        game.roll(3, 1)
        game.roll(2, 2)
        assert game.score(1) == 8
        assert game.score(2) == 6

    def test_more_than_ten_pins_in_a_frame(self):
        game = BowlingGame(2)
        game.roll(5,1)
        game.roll(5,2)
        with pytest.raises(ValueError) as e:
            game.roll(6,2)
        assert str(e.value) == "You can only clear 10 pins in a single frame."
    
    def test_three_rolls(self):
        game = BowlingGame(2)
        game.roll(5, 1)
        game.roll(5,2)
        game.roll(3, 1)
        game.roll(3, 2)
        game.roll(7, 1)
        game.roll(7, 2)
        assert game.score(1) == 15
        assert game.score(2) == 15

    def test_spare_includes_bonus_roll(self):
        game = BowlingGame(2)
        game.roll(5, 1)
        game.roll(5, 1)
        game.roll(7, 1)

        game.roll(4, 2)
        game.roll(6, 2)
        game.roll(8, 2)
        
        assert game.score(1) == 24
        assert game.score(2) == 26

    def test_spare_may_have_no_bonus(self):
        game = BowlingGame(2)
        game.roll(5, 1)
        game.roll(5, 1)
        game.roll(0, 1)

        game.roll(5, 2)
        game.roll(5, 2)
        game.roll(0, 2)
        
        assert game.score(1) == 10
        assert game.score(2) == 10

    def test_strike_includes_first_bonus_roll(self):
        game = BowlingGame(2)
        game.roll(10)
        game.roll(7)

        game.roll(10, 2)
        game.roll(7, 2)

        assert game.score() == 24
        assert game.score(2) == 24

    def test_spare_missing_next_roll(self):
        game = BowlingGame(2)
        game.roll(5)
        game.roll(5)
        assert game.score() == 10

        game.roll(5, 2)
        game.roll(5, 2)
        assert game.score(2) == 10
    
    def test_strike_could_be_followed_by_another_strike(self):
        game = BowlingGame(2)
        game.roll(10)
        game.roll(10)
        game.roll(2)
        assert game.score() == 36

        game.roll(10, 2)
        game.roll(10, 2)
        game.roll(2, 2)
        assert game.score(2) == 36

    def test_too_many_rolls(self):
        game = BowlingGame(2)
        for roll in range(20):
            game.roll(3)
            game.roll(4,2)
            
        with pytest.raises(ValueError) as e:
            game.roll(5, 2)
        assert str(e.value) == "Cannot bowl more than 10 frames."
    
    def test_too_many_rolls_on_frame_eleven(self):
        game = BowlingGame(2)
        for roll in range(20):
            game.roll(5)
            game.roll(5, 2)
            
        with pytest.raises(ValueError) as e:
            game.roll(5, 2)
            game.roll(5, 2)
            game.roll(5, 2)
        assert str(e.value) == "Cannot bowl more than 11 frames with 1 bowl for the last frame"
    
    def test_last_frame_spare_allows_bonus_roll(self):
        game = BowlingGame(2)
        for i in range(18):
            game.roll(1)
            game.roll(1, 2)
        game.roll(8)
        game.roll(8, 2)
        game.roll(1)
        game.roll(2, 2)
        game.roll(3, 2)

    def test_perfect_game(self):
        game = BowlingGame(2)
        for i in range(11):
            game.roll(10)
            game.roll(10, 2)
        assert game.score() == 300
        assert game.score(2) == 300

    def test_too_many_players(self):
        game = BowlingGame(2)
        
        with pytest.raises(ValueError) as e:
            game.roll(5, 3)
            
        assert str(e.value) == "Player number not allowed to play the game"
    
    def test_player_number_zero_or_less(self):
        game = BowlingGame(2)
        
        with pytest.raises(ValueError) as e:
            game.score(0)
            
        assert str(e.value) == "Player number not allowed to play the game"

# -------------------------------------------- Test display / simulate  -------------------------------------------------    

    def test_displayGame(self):
        game = BowlingGame(2)
        for i in range(18):
            game.roll(1)
            game.roll(1, 2)
        game.roll(8)
        game.roll(8, 2)
        game.roll(1)
        game.roll(2, 2)
        game.roll(3, 2)

        # game.displayGame()

    def test_simulate_game(self):
        game = BowlingGame(2)
        game.simulateGame()

        # game.displayGame()

    def test_simulate_game(self):
        game = BowlingGame(4)
        game.simulateGame()

        game.displayGame()

    def test_init_with_less_than_1(self):
        with pytest.raises(ValueError) as e:
            game = BowlingGame(0)
            
        assert str(e.value) == "Cannot play bowling with less than 1 player"

    def test_init_with_more_than_8(self):
        with pytest.raises(ValueError) as e:
            game = BowlingGame(10)
            
        assert str(e.value) == "Cannot play bowling with more than 8 players"
        

if __name__ == '__main__':
    unittest.main()