import random

class BowlingGame:
    def __init__(self, player = 1):
        if (player < 1):
            raise ValueError("Cannot play bowling with less than 1 player")
        if (player > 8):
            raise ValueError("Cannot play bowling with more than 8 players")
        self.player = player
        self.total = [self.scoreInit() -1] * player
        self.frames = []
        for i in range (0, player):
            self.frames.append([[]])
        self.bonus = [False] * player
        self.name = ["Alexis B.", "Alexis C.", "Hugo L.", "Maxime R.", "Milann G.", "Théo L.", "Ramy N.", "JC. Ferry"]
        random.shuffle(self.name)

    def scoreInit(self):
        return 1
    
    def roll(self, pins, player = 1):
        if (player < 1 or player > self.player):
            raise ValueError("Player number not allowed to play the game")
        player -= 1

        if pins < 0:
            raise ValueError('Cannot roll less than zero pins.')
        if pins > 10:
            raise ValueError("Cannot roll more than ten pins.")
        self.total[player] += pins
        self.frames[player][-1].append(pins)

        if self.bonus[player]:
            self.total[player] += pins

        if sum(self.frames[player][-1]) > 10:
            raise ValueError("You can only clear 10 pins in a single frame.")

        if len(self.frames[player]) > 10 and sum(self.frames[player][9]) < 10:
            raise ValueError('Cannot bowl more than 10 frames.')
        elif len(self.frames[player]) > 11:
            raise ValueError('Cannot bowl more than 11 frames.')
        elif (len(self.frames[player]) == 11 and len(self.frames[player][10]) > 1):
            raise ValueError('Cannot bowl more than 11 frames with 1 bowl for the last frame')
        
        if sum(self.frames[player][-1]) == 10:
            self.bonus[player] = True
        
        if len(self.frames[player][-1]) == 2 or pins == 10:
            self.frames[player].append([])

    def score(self, player = 1):
        if (player < 1 or player > self.player):
            raise ValueError("Player number not allowed to play the game")
        player -= 1

        total = 0
        total_frames = len(self.frames[player])
        for i in range(0, total_frames):
            frame_total = sum(self.frames[player][i])
            try: 
                if self.frames[player][i][0] == 10:
                    total += self.frames[player][i + 1][0]
                    if len(self.frames[player][i + 1]) == 2:
                            total += self.frames[player][i + 1][1]
                    else:
                        total += self.frames[player][i + 2][0]
                elif frame_total == 10:
                        total += self.frames[player][i+1][0]
            except IndexError:
                pass
            total += frame_total
        return total

    def displayGame(self):
        print(end="\n\n")
        print("Game :", end="\n\n")
        max_player = ""
        max_total = -1
        for i in range(0, self.player):
            print(self.name[i] + " : " + str(self.frames[i]) + " Total : ", end="")
            total = self.score(i+1)
            print(total)
            if (total > max_total):
                max_player = self.name[i]
                max_total = total
            elif (total == max_total):
                max_player += " and " + self.name[i] 
        
        print("\nWinner : " + max_player + " !!!", end="\n\n")
        print("----------------------------------------------------------------------")
    
    def simulateGame(self):
        for round in range (0, 10):
            for i in range(0, self.player):
                pin = random.randint(0, 10)
                pin2 = 0
                self.roll(pin, i+1)
                if (pin < 10):
                    pin2 = random.randint(0, 10 - pin)
                    self.roll(pin2, i+1)
                if (round == 9 and pin + pin2 == 10):
                    self.roll(random.randint(0, 10), i+1)

